package com.ramjanki.school.repository;

import com.ramjanki.school.entity.Students;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface StudentRepository extends JpaRepository<Students, Long> {
    @Query(value = "Select * from students where email_id = :email", nativeQuery = true)
    Optional<Students> findByEmail(@Param("email") String email);
}
