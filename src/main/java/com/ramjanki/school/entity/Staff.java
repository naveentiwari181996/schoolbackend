package com.ramjanki.school.entity;

import com.ramjanki.school.utility.JSONObjectConverter;
import org.json.JSONObject;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.List;

public class Staff {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "staff_id")
    private Long staffId;

    @NonNull
    @Column(name = "first_name", length = 20)
    private String firstName;

    @Column(name = "middle_name", length = 20)
    private String middleName;

    @Column(name = "last_name", length = 20)
    private String lastName;

    @NonNull
    @Column(name = "mobile_number", length = 13)
    private String mobileNumber;

    @Column(name = "email_id", length = 50)
    private String emailId;

    @NonNull
    @Column(name = "address", length = 150)
    private String address;

    @NonNull
    @Column(name = "qualification", length = 50)
    private String qualification;

    @NonNull
    @Column(name = "skills")
    private List<Integer> skills;

    @Column(name = "last_employment_details")
    @Convert(converter = JSONObjectConverter.class)
    private JSONObject employmentDetails;

    @NonNull
    @Column(name = "modified_by")
    private Long modifiedBy;

    @NonNull
    @Column(name = "last_modified_date_time", length = 35)
    private String lastModifiedDateTime;
}
