package com.ramjanki.school.entity;

import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Id;

public class RoleMaster {

    @Id
    @NonNull
    @Column(name = "role_id")
    private Integer roleId;

    @NonNull
    @Column(name = "role", length = 50)
    private String role;

    @NonNull
    @Column(name = "role_description", length = 150)
    private String roleDescription;


}
