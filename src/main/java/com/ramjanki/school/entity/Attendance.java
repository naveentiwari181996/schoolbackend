package com.ramjanki.school.entity;

import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class Attendance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NonNull
    @Column(name = "student_id")
    private Long studentId;

    @NonNull
    @Column(name = "date", length = 35)
    private String date;

    @NonNull
    @Column(name = "status", length = 15)
    private String status;


    @Column(name = "comments")
    private String comments;

    @NonNull
    @Column(name = "modified_by")
    private Long modifiedBy;

    @NonNull
    @Column(name = "last_modified_date_time", length = 35)
    private String lastModifiedDateTime;

}
