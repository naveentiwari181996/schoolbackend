package com.ramjanki.school.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class FeeDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "student_id")
    private Long studentId;

    @Column(name = "payment_date", length = 35)
    private String paymentDate;

    @Column(name = "payment_amount")
    private Double paymentAmount;

    @Column(name = "payment_from_month", length = 35)
    private String paymentFromMonth;

    @Column(name = "payment_to_month", length = 35)
    private String paymentToMonth;

    @Column(name = "modified_by")
    private Long modifiedBy;

    @Column(name = "last_modified_date_time", length = 35)
    private String lastModifiedDateTime;
}
