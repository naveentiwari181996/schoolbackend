package com.ramjanki.school.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class ClassMaster {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id")
    private Long sectionId;

    @Column(name = "class_name", length = 20)
    private String className;

    @Column(name = "section", length = 5)
    private String section;

    @Column(name = "medium", length = 20)
    private String medium;

}
