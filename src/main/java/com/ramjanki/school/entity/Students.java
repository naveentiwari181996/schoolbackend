package com.ramjanki.school.entity;

import org.springframework.lang.NonNull;

import javax.persistence.*;

@Entity
@Table(name = "students")
public class Students {

    @Id
    @NonNull
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long studentId;

    @Column(name = "first_name", length = 20)
    private String firstName;

    @Column(name = "middle_name", length = 20)
    private String middleName;

    @Column(name = "last_name", length = 20)
    private String lastName;

    @Column(name = "email_id", length = 50)
    private String emailId;

    @Column(name = "password")
    private String password;

    @Column(name = "address", length = 150)
    private String address;

    @Column(name = "last_modified_date_time", length = 35)
    private String lastModifiedDateTime;

    public Students() {
    }

    public Students(String firstName, String middleName, String lastName, String emailId, String password, String address, String lastModifiedDateTime) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.password = password;
        this.address = address;
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    public Students(@NonNull Long studentId, String firstName, String middleName, String lastName, String emailId, String password, String address, String lastModifiedDateTime) {
        this.studentId = studentId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.emailId = emailId;
        this.password = password;
        this.address = address;
        this.lastModifiedDateTime = lastModifiedDateTime;
    }

    @NonNull
    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(@NonNull Long studentId) {
        this.studentId = studentId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLastModifiedDateTime() {
        return lastModifiedDateTime;
    }

    public void setLastModifiedDateTime(String lastModifiedDateTime) {
        this.lastModifiedDateTime = lastModifiedDateTime;
    }
}
