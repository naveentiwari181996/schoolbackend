package com.ramjanki.school.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UtilityController {

    @GetMapping("/")
    public String ping(){
        return "Working fine";
    }
}
