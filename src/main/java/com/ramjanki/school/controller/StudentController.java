package com.ramjanki.school.controller;

import com.ramjanki.school.model.LoginStudentDto;
import com.ramjanki.school.model.StudentDetailsDto;
import com.ramjanki.school.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @PostMapping("/registerStudent")
    public String registerStudent(StudentDetailsDto studentDetailsDto){
        return studentService.registerStudent(studentDetailsDto);
    }

    @PostMapping("/loginStudent")
    public String registerStudent(LoginStudentDto loginStudentDto){
        return studentService.loginStudent(loginStudentDto);
    }
}
