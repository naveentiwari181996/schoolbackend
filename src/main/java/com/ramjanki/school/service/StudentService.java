package com.ramjanki.school.service;

import com.ramjanki.school.model.LoginStudentDto;
import com.ramjanki.school.model.StudentDetailsDto;

public interface StudentService {
    String registerStudent(StudentDetailsDto studentDetailsDto);

    String loginStudent(LoginStudentDto loginStudentDto);
}
