package com.ramjanki.school.service.impl;

import com.ramjanki.school.entity.Students;
import com.ramjanki.school.model.LoginStudentDto;
import com.ramjanki.school.model.StudentDetailsDto;
import com.ramjanki.school.repository.StudentRepository;
import com.ramjanki.school.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public String registerStudent(StudentDetailsDto studentDetailsDto) {
        Students students = new Students(studentDetailsDto.getFirstName(), studentDetailsDto.getMiddleName(), studentDetailsDto.getLastName(), studentDetailsDto.getEmail(), studentDetailsDto.getPassword(), studentDetailsDto.getAddress(), LocalDateTime.now().toString());
        studentRepository.save(students);
        return "Student Registered Successfully";
    }

    @Override
    public String loginStudent(LoginStudentDto loginStudentDto) {
        Optional<Students> students = studentRepository.findByEmail(loginStudentDto.getEmail());
        if (students.isPresent() && students.get().getPassword().equals(loginStudentDto.getPassword())){
            return "User Details matched!";
        }
        return "Invalid credentials";
    }
}
